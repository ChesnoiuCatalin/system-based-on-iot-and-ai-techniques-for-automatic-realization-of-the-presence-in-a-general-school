# USAGE

# import the necessary packages
from imutils.video import VideoStream
from flask import Response
from flask import Flask
from flask import render_template
import threading
import argparse
from datetime import datetime
import imutils
import time
import cv2
import os
import numpy as np
import sqlite3
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin

# from keras.models import model_from_json
# from keras.preprocessing import image


# initialize the output frame and a lock used to ensure thread-safe
# exchanges of the output frames (useful for multiple browsers/tabs
# are viewing the stream)
outputFrame = None
lock = threading.Lock()
faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

# initialize a flask object
app = Flask(__name__)
app.config['SECRET_KEY'] = 'ThisIsSecret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/pi/Desktop/RecunoastereFaciala/ex.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


# Databas tables ---------------------------------------------------

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(25), unique=True)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))
    account = db.Column(db.String(15))
    add_atribut = db.Column(db.String(25))


class Clase(db.Model):
    id_cl = db.Column(db.Integer, primary_key=True)
    denumire_clasa = db.Column(db.String(25))
    nume_elev = db.Column(db.String(25))


class Orar(db.Model):
    id_orar = db.Column(db.Integer, primary_key=True)
    nume_sala = db.Column(db.String(25))
    ora_start = db.Column(db.String(10))
    ora_stop = db.Column(db.String(10))
    denumire_clasa = db.Column(db.String(25))
    profesor = db.Column(db.String(25))
    materie = db.Column(db.String(25))
    ziua = db.Column(db.String(10))
    minim_minute_prez = db.Column(db.Integer)


class Prezenta(db.Model):
    id_prezenta = db.Column(db.Integer, primary_key=True)
    nume_elev = db.Column(db.String(25))
    denumire_clasa = db.Column(db.String(25))
    nume_sala = db.Column(db.String(25))
    ziua = db.Column(db.String(10))
    data = db.Column(db.String(10))
    ora_start = db.Column(db.String(10))
    ora_stop = db.Column(db.String(10))
    profesor = db.Column(db.String(25))
    materie = db.Column(db.String(25))
    prezenta = db.Column(db.String(10))


class location(db.Model):
    loc_id = db.Column(db.Integer, primary_key=True)
    locatie = db.Column(db.String(50))

# --------------------------------------------------------------------

# initialize the video stream and allow the camera sensor to
# warmup
vs = VideoStream(src=0).start()
time.sleep(2.0)


@app.route("/")
def index():
    # return the rendered template
    return render_template("videoStream.html")


def unknown(img, x, y, color):
    cv2.putText(img, "Persoana Necunoscuta", (x, y - 4), cv2.FONT_HERSHEY_SIMPLEX, 0.8, color,
                1, cv2.LINE_AA)


# Method of drawing the rectangle around the face + text
def draw_boundary(img, classifier, scaleFactor, minNeighbors, text, clf):
    color = {"blue": (255, 0, 0), "red": (0, 0, 255), "green": (0, 255, 0), "black": (0, 0, 0)}

    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    features = classifier.detectMultiScale(gray_img, scaleFactor, minNeighbors)
    coordinates = []
    for (x, y, w, h) in features:
        id, conf = clf.predict(gray_img[y:y + h, x:x + w])
        local_time = datetime.now()
        user_id = 0
        prez_finish=False
        conn = sqlite3.connect('ex.db')
        c = conn.cursor()
        for row in c.execute("SELECT COUNT(*) FROM Clase"):
            r = row
        conn.close()
        if (r[0] > 0):
            conn = sqlite3.connect('ex.db')
            c = conn.cursor()
            for row in c.execute("SELECT id_cl, denumire_clasa, nume_elev FROM Clase"):
                user_id = row[0]
                clasa_elev = row[1]
                nume_elev = row[2]
                if id == user_id:
                    cv2.rectangle(img, (x, y), (x + w, y + h), color["green"], 2)
                    cv2.putText(img, nume_elev, (x, y - 4), cv2.FONT_HERSHEY_SIMPLEX, 0.8, color["green"], 1, cv2.LINE_AA)
                    conn = sqlite3.connect('ex.db')
                    c = conn.cursor()
                    l = conn.cursor()
                    for loc in l.execute("SELECT * FROM location"):
                        current_location=loc[1]
                    current_day=local_time.strftime("%A")
                    current_hour=int(local_time.strftime("%H"))
                    current_min = int(local_time.strftime("%M"))
                    if current_day=="Monday":
                        current_day='1'
                    elif current_day=="Tuesday":
                        current_day='2'
                    elif current_day=="Wednesday":
                        current_day='3'
                    elif current_day=="Thursday":
                        current_day='4'
                    else: current_day=="Friday":
                        current_day='5'
                    for r in c.execute("SELECT * FROM Orar"):
                        hour_start=int(r[2].split(":")[0])
                        min_start=int(r[2].split(":")[1])
                        hour_stop = int(r[3].split(":")[0])
                        min_stop = int(r[3].split(":")[1])
                        if r[1]==current_location and r[7]==current_day and current_hour>=hour_start and current_hour<=hour_stop and current_min>=min_start and current_min<min_stop and r[4]==clasa_elev:
                            with open('prezenta.txt', 'r') as file_read1:
                                prez = file_read1.read()

                            curent_time = local_time.strftime("%Y-%D-%H:%M")
                            file_write = open('prezenta.txt', 'w')
                            write = prez + '\n' + nume_elev + "-PREZENT-" + curent_time
                            file_write.write(write)
                            file_write.close()


                elif id == -1:
                    cv2.rectangle(img, (x, y), (x + w, y + h), color["red"], 2)
                    cv2.putText(img, "Persoana Necunoscuta", (x, y - 4), cv2.FONT_HERSHEY_SIMPLEX, 0.8, color["red"], 1, cv2.LINE_AA)
                    cv2.putText(img, local_time.strftime("%A %d %B %Y %I:%M:%S%p"), (10, img.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 255, 0), 1)
                    curent_time = local_time.strftime("%Y-%d-%m-%y-%H:%M")
                    cv2.imwrite("/home/pi/Desktop/RecunoastereFaciala/static/pics/Necunoscut-" + curent_time + ".jpg", img)
            conn.close()
        else:
            cv2.rectangle(img, (x, y), (x + w, y + h), color["red"], 2)
            cv2.putText(img, "Persoana Necunoscuta", (x, y - 4), cv2.FONT_HERSHEY_SIMPLEX, 0.8, color["red"], 1, cv2.LINE_AA)

        coordinates = [x, y, w, h]

    return coordinates

def add_prez(nume_sala, ora_start, ora_stop, denumire_clasa, profesor, materie, ziua, minim_minute_prez):
    #Create unique lines in the prezenta.txt file
    uniqlines = set(open('prezenta.txt').readlines())
    bar = open('prezenta.txt', 'w').writelines(set(uniqlines))
    # bar.close()
    local_time = datetime.now()
    conn = sqlite3.connect('ex.db')
    c = conn.cursor()
    sql="SELECT * FROM Clase where denumire_clasa='" +denumire_clasa
    sql=sql+"'"
    clase = [[" " for x in range(100)] for y in range(3)] 
    clasa =[" " for x in range(100)]
    i=0
    j=0
    for row in c.execute(sql):
        ro = row
        clasa[i]=str(ro[2])
        print(clasa[i]+"-"+str(i))
        i=i+1
    conn.close()
    for k in range(i):
        print("k="+str(k))
        with open('prezenta.txt', 'r') as file_read2:
            count = 0
            for line in file_read2:
                prez = line.strip()
                prez_name=prez.split("-PREZENT-")[0]
                if clasa[k]==prez_name and clasa[k]!=" ":
                    count=count+1
                    print("count="+str(count))
        if count>=int(minim_minute_prez):
            data=local_time.strftime("%m/%d/%y")
            conn=sqlite3.connect('ex.db')
            a=conn.cursor()
            for row in a.execute("SELECT count(*) FROM Prezenta where denumire_clasa=? and nume_elev=? and nume_sala=? and ziua=? and data=? and ora_start=? and ora_stop=? and profesor=? and materie=?", (denumire_clasa, clasa[k], nume_sala, ziua, data, ora_start, ora_stop, profesor, materie)):
                v=row
            conn.close()
            if v[0]==0:
                new = Prezenta(nume_elev=clasa[k], denumire_clasa=denumire_clasa, nume_sala=nume_sala, ziua=ziua, data=data, ora_start=ora_start, ora_stop=ora_stop, profesor=profesor, materie=materie, prezenta="PREZENT")
                db.session.add(new)
                db.session.commit()

        elif count<int(minim_minute_prez):
            data = local_time.strftime("%m/%d/%y")
            conn=sqlite3.connect('ex.db')
            a=conn.cursor()

            for row in a.execute("SELECT count(*) FROM Prezenta where denumire_clasa=? and nume_elev=? and nume_sala=? and ziua=? and data=? and ora_start=? and ora_stop=? and profesor=? and materie=?", (denumire_clasa, clasa[k], nume_sala, ziua, data, ora_start, ora_stop, profesor, materie)):
                v=row
            conn.close()
            if v[0]==0:
                new = Prezenta(nume_elev=clasa[k], denumire_clasa=denumire_clasa, nume_sala=nume_sala, ziua=ziua, data=data, ora_start=ora_start, ora_stop=ora_stop, profesor=profesor, materie=materie, prezenta="ABSENT")
                db.session.add(new)
                db.session.commit()



def recognize(img, clf, faceCascade):
    coordinates = draw_boundary(img, faceCascade, 1.1, 10, "Persoana", clf)

    return img


def face_recognition():
    # grab global references to the video stream, output frame, and
    # lock variables
    global vs, outputFrame, lock

    clf = cv2.face.LBPHFaceRecognizer_create(2, 2, 7, 7, 15.0)
    clf.read("classifier.yml")
    ok=False
    # loop over frames from the video stream
    while True:
        # read the next frame from the video stream, resize it,
        frame = vs.read()
        frame = recognize(frame, clf, faceCascade)

        # grab the current timestamp and draw it on the frame
        timestamp = datetime.now()
        local_time = datetime.now()
        cv2.putText(frame, timestamp.strftime(
            "%A %d %B %Y %I:%M:%S%p"), (10, frame.shape[0] - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 255, 0), 1)
        conn = sqlite3.connect('ex.db')
        prez_finish=False
        c = conn.cursor()
        l = conn.cursor()
        for loc in l.execute("SELECT * FROM location"):
            current_location=loc[1]
        current_day=local_time.strftime("%A")
        current_hour=int(local_time.strftime("%H"))
        current_min = int(local_time.strftime("%M"))
        if current_day=="Monday":
            current_day='1'
        elif current_day=="Tuesday":
            current_day='2'
        elif current_day=="Wednesday":
            current_day='3'
        elif current_day=="Thursday":
            current_day='4'
        elif current_day=="Friday":
            current_day='5'
        for r in c.execute("SELECT * FROM Orar"):
            hour_start=int(r[2].split(":")[0])
            min_start=int(r[2].split(":")[1])
            hour_stop = int(r[3].split(":")[0])
            min_stop = int(r[3].split(":")[1])



            if prez_finish==False and r[1]==current_location and r[7]==current_day and current_hour==hour_stop and current_min==min_stop :
                add_prez(r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8])#pr
                file_write = open('prezenta.txt', 'w')
                file_write.write("")
                file_write.close()
                prez_finish=True
                ok=True
        conn.close()
        if ok==True:
            file_write = open('prezenta.txt', 'w')
            file_write.write("")
            file_write.close()

        # acquire the lock, set the output frame, and release the
        # lock
        with lock:
            outputFrame = frame.copy()


def generate():
    # grab global references to the output frame and lock variables
    global outputFrame, lock

    # loop over frames from the output stream
    while True:
        # wait until the lock is acquired
        with lock:
            # check if the output frame is available, otherwise skip
            # the iteration of the loop
            if outputFrame is None:
                continue

            # encode the frame in JPEG format
            (flag, encodedImage) = cv2.imencode(".jpg", outputFrame)

            # ensure the frame was successfully encoded
            if not flag:
                continue

        # yield the output frame in the byte format
        yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' +
               bytearray(encodedImage) + b'\r\n')


@app.route("/video_feed")
def video_feed():
    # return the response generated along with the specific media
    # type (mime type)
    return Response(generate(),
                    mimetype="multipart/x-mixed-replace; boundary=frame")


# check to see if this is the main thread of execution
if __name__ == '__main__':
    # start a thread that will perform motion detection
    t = threading.Thread(target=face_recognition)
    t.daemon = True
    t.start()

    # start the flask app
    app.run(host="0.0.0.0", port="8000", debug=True,
            threaded=True, use_reloader=False)

# release the video stream pointer
vs.stop()



