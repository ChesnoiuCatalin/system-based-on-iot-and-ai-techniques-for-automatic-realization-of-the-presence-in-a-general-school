import cv2
import sqlite3

from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from flask import Response
from flask import Flask
from flask import render_template

app = Flask(__name__)
app.config['SECRET_KEY'] = 'ThisIsSecret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/pi/Desktop/RecunoastereFaciala/ex.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

# Databas tables ---------------------------------------------------

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(25), unique=True)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))
    account = db.Column(db.String(15))
    add_atribut = db.Column(db.String(25))


class Clase(db.Model):
    id_cl = db.Column(db.Integer, primary_key=True)
    denumire_clasa = db.Column(db.String(25))
    nume_elev = db.Column(db.String(25))


class Orar(db.Model):
    id_orar = db.Column(db.Integer, primary_key=True)
    nume_sala = db.Column(db.String(25))
    ora_start = db.Column(db.String(10))
    ora_stop = db.Column(db.String(10))
    denumire_clasa = db.Column(db.String(25))
    profesor = db.Column(db.String(25))
    materie = db.Column(db.String(25))
    ziua = db.Column(db.String(10))
    minim_minute_prez = db.Column(db.Integer)


class Prezenta(db.Model):
    id_prezenta = db.Column(db.Integer, primary_key=True)
    nume_elev = db.Column(db.String(25))
    denumire_clasa = db.Column(db.String(25))
    nume_sala = db.Column(db.String(25))
    ziua = db.Column(db.String(10))
    data = db.Column(db.String(10))
    ora_start = db.Column(db.String(10))
    ora_stop = db.Column(db.String(10))
    profesor = db.Column(db.String(25))
    materie = db.Column(db.String(25))
    prezenta = db.Column(db.String(10))


class location(db.Model):
    loc_id = db.Column(db.Integer, primary_key=True)
    locatie = db.Column(db.String(50))


# ------------------------------------------------------------------

def generate_dataset(img, id, img_id):
    cv2.imwrite("data/user."+str(id)+"."+str(img_id)+".jpg",img)



#Metoda de trasare a dreptunghiului din jurul fetei + text
def draw_boundary(img, classifier, scaleFactor, minNeighbors, color, text):
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    features = classifier.detectMultiScale(gray_img, scaleFactor, minNeighbors)
    coordinates = []
    for (x, y, w, h) in features:
        cv2.rectangle(img, (x, y), (x+w, y+h), color, 2)
        cv2.putText(img, "Scanartse Faciala", (x, y-4), cv2.FONT_HERSHEY_SIMPLEX, 0.8, color, 1, cv2.LINE_AA)

        coordinates = [x, y, w, h]

    return coordinates

#Metoda care se ocupa cu apelarea primei metode si cu returnarea imaginii updatate dupa cautarea fetelor
def detect (img, faceCascade, eyesCascade, mouthCascade, noseCascade, img_id, user_id):
    color = {"blue":(255,0,0), "red":(0,0,255), "green":(0,255,0), "black":(0,0,0)}
    coordinates = draw_boundary(img, faceCascade, 1.1, 10, color['blue'], "Scanare Faciala")

    if len(coordinates)==4:
        roi_img = img[coordinates[1]:coordinates[1]+coordinates[3], coordinates[0]:coordinates[0]+coordinates[2]]

        generate_dataset(roi_img, user_id, img_id)

        #coordinates = draw_boundary(roi_img, eyesCascade, 1.1, 14, color['red'], "Ochi")
        #coordinates = draw_boundary(roi_img, mouthCascade, 1.1, 10, color['blue'], "Gura")
        #coordinates = draw_boundary(roi_img, noseCascade, 1.1, 12, color['black'], "Nas")


    return  img

def validate_face(img, faceCascade, img_id, user_id):
    color = {"blue":(255,0,0), "red":(0,0,255), "green":(0,255,0), "black":(0,0,0)}
    coordinates = draw_boundary(img, faceCascade, 1.1, 10, color['blue'], "Scanare Faciala")
    if len(coordinates)==4:
        ok = True
    else:
        ok = False
    return ok

faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
eyesCascade = cv2.CascadeClassifier("haarcascade_eye.xml")
mouthCascade = cv2.CascadeClassifier("Mouth.xml")
noseCascade = cv2.CascadeClassifier("nose.xml")
video_capture = cv2.VideoCapture(0)
img_id = 0

# schimba
nume_elev = input('Introduceti numele :')
denumire_clasa = input('Introduceti clasa/postul (in cazul angajatilor) :')

conn = sqlite3.connect('ex.db')
c = conn.cursor()
for row in c.execute("SELECT count(*) FROM Clase;"):
    elev_id = row[0]+1
conn.close()
#-------------


new = Clase(nume_elev=nume_elev, denumire_clasa=denumire_clasa)
db.session.add(new)
db.session.commit()
#----------------------------------------


while True:
    _, img = video_capture.read()
    img = detect(img, faceCascade, eyesCascade, mouthCascade, noseCascade, img_id, elev_id)
    cv2.imshow("Face Scaning", img)
    if validate_face(img, faceCascade, img_id, elev_id) == True:
        img_id += 1
        print(img_id)
    if cv2.waitKey(1) & img_id == 201 :
        break

video_capture.release()
cv2.destroyAllWindows()
