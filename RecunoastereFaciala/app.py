from flask import Flask, render_template, redirect, url_for
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SelectField, IntegerField
from wtforms.validators import InputRequired, Email, Length
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
import subprocess
import os
import cv2
import socket
import sqlite3
import metode
from datetime import datetime


app = Flask(__name__)
picFolder = os.path.join('static', 'pics')
app.config['UPLOAD_FOLDER'] = picFolder
app.config['SECRET_KEY'] = 'ThisIsSecret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://///home/pi/Desktop/RecunoastereFaciala/ex.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
Bootstrap(app)
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'


#Databas tables ---------------------------------------------------

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(25), unique=True)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(80))
    account = db.Column(db.String(15))
    add_atribut = db.Column(db.String(25))

class Clase(db.Model):
    id_cl = db.Column(db.Integer, primary_key=True)
    denumire_clasa = db.Column(db.String(25))
    nume_elev = db.Column(db.String(25))

class Orar(db.Model):
    id_orar = db.Column(db.Integer, primary_key=True)
    nume_sala = db.Column(db.String(25))
    ora_start = db.Column(db.String(10))
    ora_stop = db.Column(db.String(10))
    denumire_clasa = db.Column(db.String(25))
    profesor = db.Column(db.String(25))
    materie = db.Column(db.String(25))
    ziua = db.Column(db.String(10))
    minim_minute_prez = db.Column(db.Integer)

class Prezenta(db.Model):
    id_prezenta = db.Column(db.Integer, primary_key=True)
    nume_elev = db.Column(db.String(25))
    denumire_clasa = db.Column(db.String(25))
    nume_sala = db.Column(db.String(25))
    ziua = db.Column(db.String(10))
    data = db.Column(db.String(10))
    ora_start = db.Column(db.String(10))
    ora_stop = db.Column(db.String(10))
    profesor = db.Column(db.String(25))
    materie = db.Column(db.String(25))
    prezenta = db.Column(db.String(10))


class location(db.Model):
    loc_id = db.Column(db.Integer, primary_key=True)
    locatie = db.Column(db.String(50))

#------------------------------------------------------------------
#Forme-------------------------------------------------------------------------------------------

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[InputRequired(), Length(min=4, max=25)])
    password = PasswordField('Password', validators=[InputRequired(), Length(min=8, max=80)])
    account = SelectField('Account tipe', choices=[('Administrator', 'Administrator'), ('Profesor', 'Profesor'), ('Parinte', 'Parinte'), ('Paznic', 'Paznic')])
    remember = BooleanField('Remember me')

class RegisterForm(FlaskForm):
    username = StringField('Username', validators=[InputRequired(), Length(min=4, max=25)])
    email = StringField('Email', validators=[InputRequired(), Email(message='Invalid email'), Length(max=50)])
    password = PasswordField('Password', validators=[InputRequired(), Length(min=8, max=80)])
    account = SelectField('Account tipe', choices=[('Administrator', 'Administrator'), ('Profesor', 'Profesor'), ('Parinte', 'Parinte'), ('Paznic', 'Paznic')])
    add_atribut = StringField('Aditional Atribut', validators=[Length(max=50)])


class ModifyForm(FlaskForm):
    username = StringField('Username', validators=[Length(max=25)])
    email = StringField('Email',[Length(max=50)])
    password = PasswordField('Password', validators=[Length(max=80)])
    account = SelectField('Account tipe', choices=[('Administrator', 'Administrator'), ('Profesor', 'Profesor'), ('Parinte', 'Parinte'), ('Paznic', 'Paznic')])
    add_atribut = StringField('Aditional Atribut', validators=[Length(max=50)])

class SetLocation(FlaskForm):
    new_location = StringField('New Location', validators=[Length(max=50)])

class setOrar(FlaskForm):
    nume_sala = StringField('Sala de clasa', validators=[Length(min=2, max=25)])
    ora_start = StringField('Ora incepe la (oră:minute) ', validators=[Length(min=5, max=5)])
    ora_stop = StringField('Ora se termina la (oră:minute) ', validators=[Length(min=5, max=5)])
    denumire_clasa = StringField('Clasa', validators=[Length(min=2, max=25)])
    profesor = StringField('Nume Profesor', validators=[Length(min=4, max=25)])
    materie = StringField('Materie', validators=[Length(min=2, max=25)])
    ziua = SelectField('Ziua', choices=[('1', 'Luni'), ('2', 'Marti'), ('3', 'Miercuri'), ('4', 'Joi'), ('5', 'Vineri')])
    minim_minute_prez = IntegerField('Numarul minim de minute necesare stabilirii prezentei ')

class setPrezenta(FlaskForm):
    nume_elev = StringField('Nume Elev', validators=[Length(min=2, max=25)])
    denumire_clasa = StringField('Clasa din care face parte elevul', validators=[Length(min=2, max=25)])
    nume_sala = StringField('Sala de clasa', validators=[Length(min=2, max=25)])
    data = StringField('Data', validators=[Length(min=5, max=10)])
    ora_start = StringField('Ora la care a inceput cursul (oră:minute)', validators=[Length(min=5, max=5)])

class filterPrezenta(FlaskForm):
    nume_elev=StringField('Nume Elev', validators=[Length( max=25)])
    denumire_clasa = StringField('Clasa din care face parte elevul', validators=[Length( max=25)])
    nume_sala = StringField('Sala de clasa', validators=[Length( max=25)])
    data = StringField('Data (lună/zi/an)', validators=[Length( max=10)])
    profesor = StringField('Profesor', validators=[Length( max=25)])
    materie = StringField('Materie', validators=[Length( max=25)])

class FaceScan(FlaskForm):
    nume_elev = StringField('Name', validators=[Length(max=25)])
    denumire_clasa = StringField('Class/Additional Atribut', validators=[Length(max=25)])

class ElimOrar(FlaskForm):
    ziua = SelectField('Ziua', choices=[('1', 'Luni'), ('2', 'Marti'), ('3', 'Miercuri'), ('4', 'Joi'), ('5', 'Vineri')])
    nume_sala = StringField('Sala de clasa', validators=[Length(min=2, max=25)])
    ora_start = StringField('Ora incepe la (oră:minute) ', validators=[Length(min=5, max=5)])
    ora_stop = StringField('Ora se termina la (oră:minute) ', validators=[Length(min=5, max=5)])
    denumire_clasa = StringField('Clasa', validators=[Length(min=2, max=25)])

#--------------------------------------------------------------------------------------------------

@app.route('/')
def index():
    return render_template('index.html')

def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

@app.route('/eliminare', methods=['GET', 'POST'])
@login_required
def eliminare():
    ip=get_ip_address()
    return render_template('eliminare.html', ip="http://"+ip+":8000", deleteCurs="http://"+ip+":5000/deleteOrar")

@app.route('/deleteOra', methods=['GET', 'POST'])
@login_required
def deleteOra():
    form=ElimOrar()
    ip = get_ip_address()
    return render_template('deleteOrar.html', form=form, ip="http://"+ip+":8000")


@app.route('/deleteOrar', methods=['GET', 'POST'])
@login_required
def deleteOrar():
    form=ElimOrar()
    
    print("11")
    if form.ora_start.data[2]==":" and form.ora_stop.data[2]==":":
        if RepresentsInt(form.ora_start.data[0])==True and RepresentsInt(form.ora_start.data[1])==True and RepresentsInt(form.ora_start.data[3])==True and RepresentsInt(form.ora_start.data[4])==True:
            if RepresentsInt(form.ora_stop.data[0])==True and RepresentsInt(form.ora_stop.data[1])==True and RepresentsInt(form.ora_stop.data[3])==True and RepresentsInt(form.ora_stop.data[4])==True:
                print("OK")
                    #conn=sqlite3.connect('ex.db')
                    #c=conn.cursor()
                    #for row in c.execute('delete table from Orar where nume_sala=? and ziua=? and ora_start=? and ora_stop=?', (form.nume_sala.data, form.ziua.data, form.ora_start.data, form.ora_stop.data)):
                    #    r=row
                    #conn.close()
                    #del = Orar.query.filter_by(username=form.username.data).first()

                de=Orar.query.filter_by(ziua=form.ziua.data, nume_sala=form.nume_sala.data, ora_start=form.ora_start.data, ora_stop=form.ora_stop.data).first()
                db.session.delete(de)
                db.session.commit()
                return render_template('Succes.html', mesaj="Curs eliminat cu succes !")
            else:
                return render_template('error1.html', mesaj="Eroare la campul 3 -'Ora se termina la'")
#"<h1>Eroare ora sfarsit</h1>"
        else:
            return render_template('error1.html', mesaj="Eroare la campul 2 - 'Ora incepe la'")
    else:
        return render_template('error1.html', mesaj="Eroare format orar nerespectat")
    ip = get_ip_address()
    return render_template('deleteOrar.html', form=form, ip="http://"+ip+":8000")



@app.route('/deleteAllOrar', methods=['GET', 'POST'])
@login_required
def deleteAllOrar():
    db.session.query(Orar).delete()
    db.session.commit()
    return render_template('Succes.html', mesaj="Toate cursurile cu succesdin orar !")



@app.route('/showPrezenta', methods=['GET', 'POST'])
@login_required
def showPrezenta():
    conn = sqlite3.connect('ex.db')
    c = conn.cursor()
    s="<table style='border:1px solid red'>"
    s=s+"<thead class='thead-dark'><tr><th>  ID  </th><th>  Elev  </th><th>  Clasa  </th><th>  Sala  </th><th>  Ziua  </th><th>  Data  </th><th>  Ora Inceput  </th><th>  Ora Sfarsit  </th><th>  Profesor  </th><th>  Materie  </th></tr></thead>"
    for row in c.execute('SELECT * FROM Prezenta ORDER BY ziua , ora_start'):
        s=s+"<tr>"
        for x in row:
            s=s+"<td>"
            if x == '1':
                s = s+"Lumni"
            elif x == '2':
                s = s + 'Marti'
            elif x == '3':
                s = s + 'Miercuri'
            elif x == '4':
                s = s + 'Joi'
            elif x == '5':
                s = s + 'Vineri'
            else:
                s=s+str(x)
            s=s+"</td>"
        s=s+"</tr>"
    conn.close()
    style="table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}"
    style=style+"td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}"
    style=style+"tr:nth-child(even) {background-color: #dddddd;}"
    return "<html><head><style>"+style+"</style></head><body>" + s + "</body></html>"

@app.route('/deleteAllPrez', methods=['GET', 'POST'])
@login_required
def deleteAllPrez():
    db.session.query(Prezenta).delete()
    db.session.commit()
    return "<h1>Delete all from Prezenta</h1>"

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            if check_password_hash(user.password, form.password.data):
                if user.account == form.account.data:
                    login_user(user, remember=form.remember.data)
                    if user.account == "Administrator":
                        return redirect(url_for('dashboard'))
                    elif user.account == "Profesor":
                        return redirect (url_for('dashboardProf'))
                    elif user.account == "Parinte":
                        return redirect (url_for('dashboardParinte'))
                    elif user.account == "Paznic":
                        return redirect (url_for('dashboardPaznic'))

        return render_template('errlogin.html')
        #return'<h1>' + form.username.data + ' ' + form.password.data + str(form.remember.data) + form.account.data + '<h1>'
    return render_template('login.html', form = form)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = RegisterForm()
    if form.validate_on_submit():
        hashed_password = generate_password_hash(form.password.data, method='sha256')
        new_user = User(username=form.username.data, email=form.email.data, password=hashed_password, account=form.account.data, add_atribut=form.add_atribut.data)
        db.session.add(new_user)
        db.session.commit()
        return render_template('Succes.html', mesaj="New user has been created !")
        #return'<h1>' + form.username.data + ' ' + form.password.data + form.email.data + form.account.data + '<h1>'
    return render_template('signup.html', form = form)

@app.route('/contactus')
def contactus():
    return render_template('contactus.html')

@app.route('/errlogin')
def errlogin():
    return render_template('errlogin.html')

def get_ip_address():
	ip_address = '';
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("8.8.8.8",80))
	ip_address = s.getsockname()[0]
	s.close()
	return ip_address

@app.route('/dashboard')
@login_required
def dashboard():
	current_location = location.query.filter_by(loc_id=1).first()
	ip = get_ip_address()
	return render_template('dashboard.html', name=current_user.username, ip="http://"+ip+":8000", location=current_location.locatie)

@app.route('/dashboardProf')
@login_required
def dashboardProf():
        current_location = location.query.filter_by(loc_id=1).first()
        ip = get_ip_address()
        return render_template('dashboardProf.html', name=current_user.username, ip="http://"+ip+":8000", location=current_location.locatie)

@app.route('/dashboardParinte')
@login_required
def dashboardParinte():
        current_location = location.query.filter_by(loc_id=1).first()
        ip = get_ip_address()
        return render_template('dashboardParinte.html', name=current_user.username, ip="http://"+ip+":8000", location=current_location.locatie)

@app.route('/dashboardPaznic')
@login_required
def dashboardPaznic():
        current_location = location.query.filter_by(loc_id=1).first()
        ip = get_ip_address()
        return render_template('dashboardPaznic.html', name=current_user.username, ip="http://"+ip+":8000", location=current_location.locatie)

@app.route('/FacialScaning')
@login_required
def FacialScaning():
        ip = get_ip_address()
        return render_template('FacialScaning.html', ip="http://"+ip+":8000")


@app.route('/profile')
@login_required
def profile():
    ip = get_ip_address()
    if current_user.account=='Administrator':
        return render_template('profile2.html', ip="http://"+ip+":8000", id=current_user.id, name=current_user.username, email=current_user.email, password=current_user.password, account=current_user.account)
    elif current_user.account=='Paznic':
                return render_template('profilePaznic.html', ip="http://"+ip+":8000", id=current_user.id, name=current_user.username, email=current_user.email, password=current_user.password,  account=current_user.account)
    elif current_user.account=='Profesor':
        return render_template('profileProf.html', ip="http://"+ip+":8000",id=current_user.id, name=current_user.username, email=current_user.email, password=current_user.password, account=current_user.account, add_atribut=current_user.add_atribut)
    elif current_user.account=='Parinte':
        return render_template('profileParinte.html', ip="http://"+ip+":8000",id=current_user.id, name=current_user.username, email=current_user.email, password=current_user.password, account=current_user.account, add_atribut=current_user.add_atribut)

@app.route('/modify', methods=['GET', 'POST'])
@login_required
def modify():
    form = ModifyForm()
    return render_template('modify.html', form=form)

@app.route('/modify_up', methods=['GET', 'POST'])
@login_required
def modify_up():
    form = ModifyForm()
    #if form.validate_on_submit():
    #if form.validate_on_submit():
    if form.username.data!="":
        User.query.filter_by(id=current_user.id).update(dict(username=form.username.data))
        db.session.commit()
        return render_template('Succes.html', mesaj="Update was Successful !")


    if form.email.data!="" and form.email.data!=current_user.email:
        User.query.filter_by(id=current_user.id).update(dict(email=form.email.data))
        db.session.commit()
        return render_template('Succes.html', mesaj="Update was Successful !")


    if form.password.data!="" and form.password.data!=current_user.password:
        hashed_password = generate_password_hash(form.password.data, method='sha256')
        User.query.filter_by(id=current_user.id).update(dict(password=hashed_password))
        db.session.commit()
        return render_template('Succes.html', mesaj="Update was Successful !")
        print("start")
    return render_template('modify.html', form=form)

@app.route('/delete')
@login_required
def delete():
    db.session.delete(current_user)
    db.session.commit()
    logout_user()
    return redirect(url_for('index'))


@app.route('/set_location', methods=['GET', 'POST'])
@login_required
def set_location():
    form = SetLocation()
    ip = get_ip_address()
    #if form.validate_on_submit():
    current_location = location.query.filter_by(loc_id=1).first()
    return render_template('set_location.html', old_location=current_location.locatie, form=form, ip="http://"+ip+":8000")

@app.route('/set_location2', methods=['GET', 'POST'])
@login_required
def set_location2():
    form = SetLocation()
    #if form.validate_on_submit():
    current_location = location.query.filter_by(loc_id=1).first()
    if form.new_location.data!="" :
        location.query.filter_by(loc_id=1).update(dict(locatie=form.new_location.data))
        db.session.commit()
        current_location = location.query.filter_by(loc_id=1).first()
        return render_template('set_location.html', old_location=current_location.locatie, form=form)
    return render_template('set_location.html', old_location=current_location.locatie,form=form)

@app.route('/Orarul', methods=['GET', 'POST'])
@login_required
def Orarul():
    form = setOrar()
    ip = get_ip_address()
    if form.validate_on_submit():
        rez=Orar.query.filter_by(nume_sala=form.nume_sala.data, ora_start=form.ora_start.data, ora_stop=form.ora_stop.data, ziua=form.ziua.data).first()
        if form.ora_start.data[2]==":" and form.ora_stop.data[2]==":":
            if RepresentsInt(form.ora_start.data[0])==True and RepresentsInt(form.ora_start.data[1])==True and RepresentsInt(form.ora_start.data[3])==True and RepresentsInt(form.ora_start.data[4])==True:
                if RepresentsInt(form.ora_stop.data[0])==True and RepresentsInt(form.ora_stop.data[1])==True and RepresentsInt(form.ora_stop.data[3])==True and  RepresentsInt(form.ora_stop.data[4])==True :
                    ora_start=int(form.ora_start.data[0] + form.ora_start.data[1])
                    min_start=int(form.ora_start.data[3] + form.ora_start.data[4])
                    ora_stop=int(form.ora_stop.data[0] + form.ora_stop.data[1])
                    min_stop=int(form.ora_stop.data[3] + form.ora_stop.data[4])
                    max_interval = (ora_stop-ora_start)*60+(min_stop-min_start)
                    if max_interval>0:
                        if form.minim_minute_prez.data<=max_interval:
                            if rez==None:
                                ora = Orar(nume_sala=form.nume_sala.data, ora_start=form.ora_start.data, ora_stop=form.ora_stop.data, denumire_clasa=form.denumire_clasa.data, profesor=form.profesor.data, materie=form.materie.data, ziua=form.ziua.data, minim_minute_prez=form.minim_minute_prez.data)
                                db.session.add(ora)
                                db.session.commit()
                                return render_template('Succes.html', mesaj="Cursul a fost adaugat cu succes !")
                            else:
                                Orar.query.filter_by(nume_sala=form.nume_sala.data, ora_start=form.ora_start.data, ora_stop=form.ora_stop.data, ziua=form.ziua.data).update(dict(denumire_clasa=form.denumire_clasa.data, profesor=form.profesor.data, materie=form.materie.data, minim_minute_prez=form.minim_minute_prez.data))
                                db.session.commit()
                                return render_template('Succes.html', mesaj="Cursul a fost modificat !")
                        else:
                            return render_template('error1.html', mesaj="Numarul minim de minute nu poate depasi"+str(max_interval)+"de minute")
                    else:
                        return render_template('error1.html', mesaj="Eroare , ora la care se incheie cursul trebuie sa fie mai mare decat ora la care incepe")
                else:
                    return render_template('error1.html', mesaj="Eroare in campul 'Ora se termina la'")
            else:
                return render_template('error1.html', mesaj="Eroare in campul 'Ora incepe la'")
        else:
            return render_template('error1.html', mesaj="Eroare in campurile ora")
    return render_template('orar.html', form = form, ip="http://"+ip+":8000")

@app.route('/showOrarul', methods=['GET', 'POST'])
@login_required
def showOrarul():
    conn = sqlite3.connect('ex.db')
    c = conn.cursor()
    s="<table style='border:1px solid red'>"
    s=s+"<thead class='thead-dark'><tr><th>Sala  </th><th>  Ziua  </th><th>  Ora  </th><th>  Clasa  </th><th>  Profesor  </th><th>  Materie  </th></tr></thead>"
    for row in c.execute('SELECT nume_sala,ziua,ora_start,denumire_clasa,profesor,materie FROM Orar ORDER BY ziua , ora_start'):
        s=s+"<tr>"
        for x in row:
            s=s+"<td>"
            if x == '1':
                s = s+"Lumni"
            elif x == '2':
                s = s + 'Marti'
            elif x == '3':
                s = s + 'Miercuri'
            elif x == '4':
                s = s + 'Joi'
            elif x == '5':
                s = s + 'Vineri'
            else:
                s=s+str(x)
            s=s+"</td>"
        s=s+"</tr>"
    conn.close()
    style="table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}"
    style=style+"td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}"
    style=style+"tr:nth-child(even) {background-color: #dddddd;}"
    return "<html><head><style>"+style+"</style></head><body>" + s + "</body></html>"

@app.route('/showOrarProf', methods=['GET', 'POST'])
@login_required
def showOrarProf():
    conn = sqlite3.connect('ex.db')
    c = conn.cursor()
    s="<table style='border:1px solid red'>"
    s=s+"<thead class='thead-dark'><tr><th>Sala  </th><th>  Ziua  </th><th>  Ora  </th><th>  Clasa  </th><th>  Profesor  </th><th>  Materie  </th></tr></thead>"
    sql="SELECT nume_sala,ziua,ora_start,denumire_clasa,profesor,materie FROM Orar where profesor='"+current_user.username
    sql=sql+"' and materie='"+current_user.add_atribut
    sql=sql+"' ORDER BY ziua , ora_start"
    print(sql)
    for row in c.execute(sql):
        s=s+"<tr>"
        for x in row:
            s=s+"<td>"
            if x == '1':
                s = s+"Lumni"
            elif x == '2':
                s = s + 'Marti'
            elif x == '3':
                s = s + 'Miercuri'
            elif x == '4':
                s = s + 'Joi'
            elif x == '5':
                s = s + 'Vineri'
            else:
                s=s+str(x)
            s=s+"</td>"
        s=s+"</tr>"
    conn.close()
    style="table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}"
    style=style+"td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}"
    style=style+"tr:nth-child(even) {background-color: #dddddd;}"
    return "<html><head><style>"+style+"</style></head><body>" + s + "</body></html>"

@app.route('/showOrarParinte', methods=['GET', 'POST'])
@login_required
def showOrarParinte():
    conn = sqlite3.connect('ex.db')
    c = conn.cursor()
    sql2="select denumire_clasa from Clase where nume_elev='"+current_user.add_atribut
    sql2=sql2+"' "
    for row in c.execute(sql2):
        den_clasa=row[0]

    s="<table style='border:1px solid red'>"
    s=s+"<thead class='thead-dark'><tr><th>Sala  </th><th>  Ziua  </th><th>  Ora  </th><th>  Clasa  </th><th>  Profesor  </th><th>  Materie  </th></tr></thead>"
    sql="SELECT nume_sala,ziua,ora_start,denumire_clasa,profesor,materie FROM Orar where denumire_clasa='"+den_clasa
    sql=sql+"' ORDER BY ziua , ora_start"

    for row in c.execute(sql):
        s=s+"<tr>"
        for x in row:
            s=s+"<td>"
            if x == '1':
                s = s+"Lumni"
            elif x == '2':
                s = s + 'Marti'
            elif x == '3':
                s = s + 'Miercuri'
            elif x == '4':
                s = s + 'Joi'
            elif x == '5':
                s = s + 'Vineri'
            else:
                s=s+str(x)
            s=s+"</td>"
        s=s+"</tr>"
    conn.close()
    style="table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}"
    style=style+"td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}"
    style=style+"tr:nth-child(even) {background-color: #dddddd;}"
    return "<html><head><style>"+style+"</style></head><body>" + s + "</body></html>"




@app.route('/filter_prez', methods=['GET', 'POST'])
@login_required
def filter_prez():
    form=filterPrezenta()
    ip = get_ip_address()
    if form.validate_on_submit():
        if form.nume_elev.data!="" or form.profesor.data!="" or form.materie.data!="" or form.denumire_clasa.data!="" or form.data.data!="" or form.nume_sala!="":
            sql='select nume_elev , profesor , materie , denumire_clasa , nume_sala , ziua , data , ora_start , prezenta from Prezenta '
            init=False
            #print(form.nume_elev.data)
            if form.nume_elev.data!='' and init==False:
                sql=sql+" where nume_elev='"
                sql=sql+form.nume_elev.data+"' "
                init=True
            if form.profesor.data!='' and init==False:
                sql=sql+" where profesor='"
                sql=sql+form.profesor.data+"' "
                init=True
            elif form.profesor.data!='' and init==True:
                sql=sql+" and profesor='"
                sql=sql+form.profesor.data+"' "
            if form.materie.data!='' and init==False:
                sql=sql+" where materie='"
                sql=sql+form.materie.data+"' "
                init=True
            elif form.materie.data!='' and init==True:
                sql=sql+" and materie='"
                sql=sql+form.materie.data+"' "
            if form.denumire_clasa.data!='' and init==False:
                sql=sql+" where denumire_clasa='"
                sql=sql+form.denumire_clasa.data+"' "
                init=True
            elif form.denumire_clasa.data!='' and init==True:
                sql=sql+" and denumire_clasa='"
                sql=sql+form.denumire_clasa.data+"' "
            if form.nume_sala.data!='' and init==False:
                sql=sql+" where nume_sala='"
                sql=sql+form.nume_sala.data+"' "
                init=True
            elif form.nume_sala.data!='' and init==True:
                sql=sql+" and nume_sala='"
                sql=sql+form.nume_sala.data+"' "
            if form.data.data!='' and init==False:
                sql=sql+" where data='"
                sql=sql+form.data.data+"' "
                init=True
            elif form.data.data!='' and init==True:
                sql=sql+" and data='"
                sql=sql+form.data.data+"' "
            #print (sql)
            conn = sqlite3.connect('ex.db')
            c = conn.cursor()
            s="<table style='border:1px solid red'>"
            s=s+"<thead class='thead-dark'><tr><th>  Elev  </th><th>  Profesor  </th><th>  Materie  </th><th>  Clasa  </th><th>  Sala  </th><th>  Ziua  </th><th>  Data  </th><th>  Ora  </th><th>  Prezenta  </th></tr></thead>"
            for row in c.execute(sql):
                s=s+"<tr>"
                for x in row:
                    s=s+"<td>"
                    if x == '1':
                        s = s+"Lumni"
                    elif x == '2':
                        s = s + 'Marti'
                    elif x == '3':
                        s = s + 'Miercuri'
                    elif x == '4':
                        s = s + 'Joi'
                    elif x == '5':
                        s = s + 'Vineri'
                    else:
                        s=s+str(x)
                    s=s+"</td>"
                s=s+"</tr>"
            conn.close()
            style="table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}"
            style=style+"td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}"
            style=style+"tr:nth-child(even) {background-color: #dddddd;}"
            return "<html><head><style>"+style+"</style></head><body>" + s + "</body></html>"
        else:
            return "<h1>eroare prezente</h1>"
    return render_template('filterPrez.html', form=form, ip="http://"+ip+":8000")



@app.route('/prezTotal', methods=['GET', 'POST'])
@login_required
def prezTotal():
     sql='select nume_elev , profesor , materie , denumire_clasa , nume_sala , ziua , data , ora_start , prezenta from Prezenta '
     conn = sqlite3.connect('ex.db')
     c = conn.cursor()
     s="<table style='border:1px solid red'>"
     s=s+"<thead class='thead-dark'><tr><th>  Elev  </th><th>  Profesor  </th><th>  Materie  </th><th>  Clasa  </th><th>  Sala  </th><th>  Ziua  </th><th>   Data  </th><th>  Ora  </th><th>  Prezenta  </th></tr></thead>" 
     for row in c.execute(sql):
         s=s+"<tr>"
         for x in row:
             s=s+"<td>"
             if x == '1':
                 s = s+"Lumni"
             elif x == '2':
                 s = s + 'Marti'
             elif x == '3':
                 s = s + 'Miercuri'
             elif x == '4':
                 s = s + 'Joi'
             elif x == '5':
                 s = s + 'Vineri'
             else:
                 s=s+str(x)
             s=s+"</td>"
         s=s+"</tr>"
     conn.close()
     style="table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}"
     style=style+"td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}"
     style=style+"tr:nth-child(even) {background-color: #dddddd;}"
     return "<html><head><style>"+style+"</style></head><body>" + s + "</body></html>"



@app.route('/filterPrezProf', methods=['GET', 'POST'])
@login_required
def filterPrezProf():
    form=filterPrezenta()
    ip = get_ip_address()
    if form.validate_on_submit():
        if form.nume_elev.data!=None or form.denumire_clasa.data!=None or form.data.data!=None or form.nume_sala.data!=None :
            sql="select nume_elev , profesor , materie , denumire_clasa , nume_sala , ziua , data , ora_start , prezenta from Prezenta where profesor='"+current_user.username
            sql=sql+"' and materie='"+current_user.add_atribut
            sql=sql+"' "
            if form.nume_elev.data!='' :
                sql=sql+" and nume_elev='"
                sql=sql+form.nume_elev.data+"' "
            if form.denumire_clasa.data!='' :
                sql=sql+" and denumire_clasa='"
                sql=sql+form.denumire_clasa.data+"' "
            if form.nume_sala.data!='' :
                sql=sql+" and nume_sala='"
                sql=sql+form.nume_sala.data+"' "
            if form.data.data!='' :
                sql=sql+" and data='"
                sql=sql+form.data.data+"' "

            conn = sqlite3.connect('ex.db')
            c = conn.cursor()
            s="<table style='border:1px solid red'>"
            s=s+"<thead class='thead-dark'><tr><th>  Elev  </th><th>  Profesor  </th><th>  Materie  </th><th>  Clasa  </th><th>  Sala  </th><th>  Ziua  </th><th>  Data  </th><th>  Ora  </th><th>  Prezenta  </th></tr></thead>"

            for row in c.execute(sql):
                s=s+"<tr>"
                for x in row:
                    s=s+"<td>"
                    if x == '1':
                        s = s+"Lumni"
                    elif x == '2':
                        s = s + 'Marti'
                    elif x == '3':
                        s = s + 'Miercuri'
                    elif x == '4':
                        s = s + 'Joi'
                    elif x == '5':
                        s = s + 'Vineri'
                    else:
                        s=s+str(x)
                    s=s+"</td>"
                s=s+"</tr>"
            conn.close()
            style="table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}"
            style=style+"td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}"
            style=style+"tr:nth-child(even) {background-color: #dddddd;}"
            return "<html><head><style>"+style+"</style></head><body>" + s + "</body></html>"
        else:
            return "<h1>eroare prezente</h1>"
    return render_template('filterPrezProf.html', form=form, ip="http://"+ip+":8000")


@app.route('/filterPrezParinte', methods=['GET', 'POST'])
@login_required
def filterPrezParinte():
    form=filterPrezenta()
    ip = get_ip_address()
    if form.validate_on_submit():
        if form.nume_elev.data!=None or form.denumire_clasa.data!=None or form.data.data!=None or form.nume_sala.data!=None :
            sql="select nume_elev , profesor , materie , denumire_clasa , nume_sala , ziua , data , ora_start , prezenta from Prezenta where nume_elev='"+current_user.add_atribut
            sql=sql+"' "
            if form.profesor.data!='' :
                sql=sql+" and profesor='"
                sql=sql+form.profesor.data+"' "
            if form.materie.data!='' :
                sql=sql+" and materie='"
                sql=sql+form.materie.data+"' "
            if form.nume_sala.data!='' :
                sql=sql+" and nume_sala='"
                sql=sql+form.nume_sala.data+"' "
            if form.data.data!='' :
                sql=sql+" and data='"
                sql=sql+form.data.data+"' "
            print(sql)
            conn = sqlite3.connect('ex.db')
            c = conn.cursor()
            s="<table style='border:1px solid red'>"
            s=s+"<thead class='thead-dark'><tr><th>  Elev  </th><th>  Profesor  </th><th>  Materie  </th><th>  Clasa  </th><th>  Sala  </th><th>  Ziua  </th><th>  Data  </th><th>  Ora  </th><th>  Prezenta  </th></tr></thead>"

            for row in c.execute(sql):
                s=s+"<tr>"
                for x in row:
                    s=s+"<td>"
                    if x == '1':
                        s = s+"Lumni"
                    elif x == '2':
                        s = s + 'Marti'
                    elif x == '3':
                        s = s + 'Miercuri'
                    elif x == '4':
                        s = s + 'Joi'
                    elif x == '5':
                        s = s + 'Vineri'
                    else:
                        s=s+str(x)
                    s=s+"</td>"
                s=s+"</tr>"
            conn.close()
            style="table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}"
            style=style+"td, th {border: 1px solid #dddddd;text-align: left;padding: 8px;}"
            style=style+"tr:nth-child(even) {background-color: #dddddd;}"
            return "<html><head><style>"+style+"</style></head><body>" + s + "</body></html>"
        else:
            return render_template('error1.html', mesaj="Eroare prezente")
    return render_template('filterPrezParinte.html', form=form, ip="http://"+ip+":8000")




@app.route('/set_prez', methods=['GET', 'POST'])
@login_required
def set_prez():
    ip=get_ip_address()
    form=setPrezenta()
    if form.validate_on_submit():
        if form.nume_elev.data!=None and form.denumire_clasa.data!=None and form.data.data!=None and form.ora_start.data!=None :
            conn = sqlite3.connect('ex.db')
            c = conn.cursor()
            for row in c.execute("SELECT COUNT(*) FROM Prezenta"):
                r=row
            conn.close()
            if (r[0]>0):
                sql="SELECT COUNT(*) FROM Prezenta where nume_elev='"+form.nume_elev.data
                sql=sql+"' "
                sql=sql+"and denumire_clasa='"+form.denumire_clasa.data
                sql=sql+"' "
                sql=sql+"and data='"+form.data.data
                sql=sql+"' "
                sql=sql+"and ora_start='"+form.ora_start.data
                sql=sql+"' "
                sql=sql+"and profesor='"+current_user.username
                sql=sql+"' "
                sql=sql+"and materie='"+current_user.add_atribut
                sql=sql+"' "
                print(sql)
                conn = sqlite3.connect('ex.db')
                c2 = conn.cursor()
                for row in c2.execute(sql):
                    r = row
                    print(r)
                conn.close()
                if(r[0]>0):
                    Prezenta.query.filter_by(nume_elev=form.nume_elev.data, denumire_clasa=form.denumire_clasa.data,
                                         data=form.data.data, ora_start=form.ora_start.data, profesor=current_user.username, materie=current_user.add_atribut).update(
                        dict(prezenta="PREZENT"))
                    db.session.commit()
                    return render_template('Succes.html', mesaj="Prezenta trecuta cu succes!")
                else:
                    return render_template('error1.html', mesaj="Nu a existat nicio ora de "+current_user.add_atribut+" pe data de "+form.data.data+" la ora "+form.ora_start.data)
            else:
                return render_template('error1.html', mesaj="Table Prezenta is empty")

        else:
            return "<h1>Eroare</h1>"
    return render_template('set_prez.html', form = form, ip='http://'+ip+':8000')

@app.route('/face_scan', methods=['GET', 'POST'])
@login_required
def face_scan():
    form = FaceScan()
    return render_template('face_scaning.html', form=form)

@app.route('/face_scan2', methods=['GET', 'POST'])
@login_required
def face_scan2():
    form = FaceScan()
    ip = get_ip_address()
    if form.nume_elev.data!="" and form.denumire_clasa.data!="" and form.nume_elev.data!=None and form.denumire_clasa.data!=None:
        new = Clase(nume_elev=form.nume_elev.data, denumire_clasa=form.denumire_clasa.data)
        db.session.add(new)
        db.session.commit()
        user_id=metode.corelare_nume(form.nume_elev.data)
        ip = get_ip_address()

        return 'http://'+ip+':8000'
    else:
        return "<h1>Erroare Date</h1>"
    return render_template('face_scaning.html', form=form, ip = 'http://'+ip+':8000')



@app.route('/gallery')
@login_required
def gallery():
    path = "/home/pi/Desktop/RecunoastereFaciala/static/pics"
    imageList = os.listdir(path)
    imagelist = ['pics/' + image for image in imageList]
    return render_template("gallery.html", imagelist=imagelist)

@app.route('/deleteGall', methods=['GET', 'POST'])
@login_required
def deleteGall():
    ip = get_ip_address()
    return render_template("deleteGallery.html", ip="http://"+ip+":8000")

@app.route('/deleteGallery', methods=['GET', 'POST'])
@login_required
def deleteGallery():
    subprocess.call("rm /home/pi/Desktop/RecunoastereFaciala/static/pics/*", shell=True)
    return render_template('Succes.html', mesaj="Delete Security Image !")

@app.route('/videoStream')
@login_required
def videoStream():
    path = "/home/pi/Desktop/RecunoastereFaciala/templates/videoStream"
    imageList = os.listdir(path)
    imagelist = ['videoStream/' + image for image in imageList]
    return render_template("VideoStream.html", imagelist=imagelist)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/test')
@login_required
def test():
    subprocess.call(['python', '/home/pi/Desktop/RecunoastereFaciala/Commands.py', 'test'])
    return '<h1>Test Command was executed !</h1>'



if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
    #subprocess.call(['flask','run','-h','192.168.1.104'])

